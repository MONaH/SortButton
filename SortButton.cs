﻿using Oxide.Core;
using Oxide.Game.Rust.Cui;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("Sort Button", "fullbanner", "0.0.7")]
    [Description("Adds a sort button to storage boxes, allowing you to sort items by name or category")]
    public class SortButton : RustPlugin
    {
        #region variables

        private Dictionary<ulong, PlayerOptions> CustomPlayerOptions => CustomStoredData.CustomPlayerOptions;
        private Dictionary<string, PluginConfig> CustomPluginConfig = new Dictionary<string, PluginConfig>();
        private StoredData CustomStoredData;
        private const string Perm = "sortbutton.use";

        private const string IdSortButton = "sbSortbutton";
        private const string IdOrderButton = "sbOrderbutton";
        private ItemContainer BackpackContainer = null;
        private ulong BackpackOwnerID = 0;

        #endregion variables

        private void Init()
        {
            permission.RegisterPermission(Perm, this);
            CustomPluginConfig = Config.ReadObject<Dictionary<string, PluginConfig>>();
            CustomStoredData = Interface.Oxide.DataFileSystem.ReadObject<StoredData>(Name);
        }

        private void OnServerInitialized()
        {
            foreach (BasePlayer player in BasePlayer.activePlayerList)
            {
                OnPlayerConnected(player);
            }
        }

        private void OnPlayerConnected(BasePlayer player)
        {
            if (!CustomPlayerOptions.ContainsKey(player.userID))
            {
                CustomPlayerOptions[player.userID] = new PlayerOptions
                {
                    SortType = "N"
                };
            }
        }

        private void OnPlayerLootEnd(PlayerLoot inventory)
        {
            BasePlayer player = inventory.GetComponent<BasePlayer>();
            CuiHelper.DestroyUi(player, IdSortButton);
            CuiHelper.DestroyUi(player, IdOrderButton);
        }
 
        private class PlayerOptions
        {
            public string SortType;
        }

        private class PluginConfig
        {
            public float OrderButtonX;
            public float OrderButtonY;
            public float OrderButtonWidth;
            public float OrderButtonHeight;

            public float SortButtonX;
            public float SortButtonY;
            public float SortButtonWidth;
            public float SortButtonHeight;
        }

        protected override void LoadDefaultConfig()
        {
            CustomPluginConfig = new Dictionary<string, PluginConfig>()
            {
                ["backpack"] = new PluginConfig
                {
                    OrderButtonX = 0.8755f,
                    OrderButtonY = 0.758f,
                    OrderButtonWidth = 0.01f,
                    OrderButtonHeight = 0.028f,
                    SortButtonX = 0.8865f,
                    SortButtonY = 0.758f,
                    SortButtonWidth = 0.06f,
                    SortButtonHeight = 0.028f
                },
                ["box.wooden.large"] = new PluginConfig
                {
                    OrderButtonX = 0.8755f,
                    OrderButtonY = 0.586f,
                    OrderButtonWidth = 0.01f,
                    OrderButtonHeight = 0.028f,
                    SortButtonX = 0.8865f,
                    SortButtonY = 0.586f,
                    SortButtonWidth = 0.06f,
                    SortButtonHeight = 0.028f
                },
                ["coffinstorage"] = new PluginConfig
                {
                    OrderButtonX = 0.8755f,
                    OrderButtonY = 0.758f,
                    OrderButtonWidth = 0.01f,
                    OrderButtonHeight = 0.028f,
                    SortButtonX = 0.8865f,
                    SortButtonY = 0.758f,
                    SortButtonWidth = 0.06f,
                    SortButtonHeight = 0.028f
                },
                ["cupboard.tool.deployed"] = new PluginConfig
                {
                    OrderButtonX = 0.8755f,
                    OrderButtonY = 0.778f,
                    OrderButtonWidth = 0.01f,
                    OrderButtonHeight = 0.028f,
                    SortButtonX = 0.8865f,
                    SortButtonY = 0.778f,
                    SortButtonWidth = 0.06f,
                    SortButtonHeight = 0.028f
                },
                ["dropbox.deployed"] = new PluginConfig
                {
                    OrderButtonX = 0.8755f,
                    OrderButtonY = 0.328f,
                    OrderButtonWidth = 0.01f,
                    OrderButtonHeight = 0.028f,
                    SortButtonX = 0.8865f,
                    SortButtonY = 0.328f,
                    SortButtonWidth = 0.06f,
                    SortButtonHeight = 0.028f
                },
                ["fridge.deployed"] = new PluginConfig
                {
                    OrderButtonX = 0.8755f,
                    OrderButtonY = 0.586f,
                    OrderButtonWidth = 0.01f,
                    OrderButtonHeight = 0.028f,
                    SortButtonX = 0.8865f,
                    SortButtonY = 0.586f,
                    SortButtonWidth = 0.06f,
                    SortButtonHeight = 0.028f
                },
                ["small_stash_deployed"] = new PluginConfig
                {
                    OrderButtonX = 0.8755f,
                    OrderButtonY = 0.242f,
                    OrderButtonWidth = 0.01f,
                    OrderButtonHeight = 0.028f,
                    SortButtonX = 0.8865f,
                    SortButtonY = 0.242f,
                    SortButtonWidth = 0.06f,
                    SortButtonHeight = 0.028f
                },
                ["woodbox_deployed"] = new PluginConfig
                {
                    OrderButtonX = 0.8755f,
                    OrderButtonY = 0.328f,
                    OrderButtonWidth = 0.01f,
                    OrderButtonHeight = 0.028f,
                    SortButtonX = 0.8865f,
                    SortButtonY = 0.328f,
                    SortButtonWidth = 0.06f,
                    SortButtonHeight = 0.028f
                }
            };
            Config.WriteObject(CustomPluginConfig, true);
        }

        protected override void LoadDefaultMessages()
        {
            // en
            lang.RegisterMessages(new Dictionary<string, string>
            {
                ["SortButtonText"] = "Sort"
            }, this, "en");

            // fr
            lang.RegisterMessages(new Dictionary<string, string>
            {
                ["SortButtonText"] = "Trier"
            }, this, "fr");

            // pt-BR
            lang.RegisterMessages(new Dictionary<string, string>
            {
                ["SortButtonText"] = "Ordenar"
            }, this, "pt-BR");

            // ru
            lang.RegisterMessages(new Dictionary<string, string>
            {
                ["SortButtonText"] = "Сортировать"
            }, this, "ru");
        }

        #region persistent data

        private void OnServerSave()
        {
            Interface.Oxide.DataFileSystem.WriteObject(Name, CustomStoredData);
        }

        private class StoredData
        {
            public Dictionary<ulong, PlayerOptions> CustomPlayerOptions { get; private set; } = new Dictionary<ulong, PlayerOptions>();
        }

        #endregion persistent data

        private void CreateButtonUI(BasePlayer player, PluginConfig button)
        {
            string OrderButtonColor = GetSortType(player) == "N" ? "0.26 0.58 0.80 0.8" : "0.75 0.43 0.18 0.8";
            CuiElementContainer elements = new CuiElementContainer {
                {
                    new CuiButton {
                        RectTransform = {
                            AnchorMin = string.Format("{0} {1}", button.OrderButtonX, button.OrderButtonY),
                            AnchorMax = string.Format("{0} {1}", button.OrderButtonX + button.OrderButtonWidth, button.OrderButtonY + button.OrderButtonHeight)
                        },
                        Button = {
                            Command = "sortbutton.order",
                            Color = OrderButtonColor
                        },
                        Text = {
                            Align = TextAnchor.MiddleCenter,
                            Text = GetSortType (player),
                            Color = "0.77 0.92 0.67 0.8",
                            FontSize = 12
                        }
                    },
                    "Overlay",
                    IdOrderButton
                },
                {
                    new CuiButton {
                        RectTransform = {
                            AnchorMin = string.Format("{0} {1}", button.SortButtonX, button.SortButtonY),
                            AnchorMax = string.Format("{0} {1}", button.SortButtonX + button.SortButtonWidth, button.SortButtonY + button.SortButtonHeight)
                        },
                        Button = {
                            Command = "sortbutton.sort",
                            Color = "0.41 0.50 0.25 0.8"
                        },
                        Text = {
                            Align = TextAnchor.MiddleCenter,
                            Text = lang.GetMessage ("SortButtonText", this, player.UserIDString),
                            Color = "0.77 0.92 0.67 0.8",
                            FontSize = 12
                        }
                    },
                    "Overlay",
                    IdSortButton
                }
            };

            CuiHelper.AddUi(player, elements);
        }

        private string GetSortType(BasePlayer player)
        {
            return CustomPlayerOptions[player.userID].SortType ?? "N";
        }

        private void RecreateSortButton(BasePlayer player)
        {
            CuiHelper.DestroyUi(player, IdSortButton);
            CuiHelper.DestroyUi(player, IdOrderButton);

            if (BackpackOwnerID != 0)
            {
                CreateButtonUI(player, CustomPluginConfig["backpack"]);
            }
            else
            {
                StorageContainer storage = player.inventory.loot?.entitySource as StorageContainer;
                OnLootEntity(player, storage);
            }
        }

        [ConsoleCommand("sortbutton.order")]
        private void ConsoleCommand_SortType(ConsoleSystem.Arg arg)
        {
            BasePlayer player = arg.Player();
            if (player == null)
                return;

            if (CustomPlayerOptions[player.userID].SortType == "N")
                CustomPlayerOptions[player.userID].SortType = "C";
            else
                CustomPlayerOptions[player.userID].SortType = "N";

            RecreateSortButton(player);
        }

        [ConsoleCommand("sortbutton.sort")]
        private void ConsoleCommand_Sort(ConsoleSystem.Arg arg)
        {
            BasePlayer player = arg.Player();
            if (player == null)
                return;

            StorageContainer lootSource = player.inventory.loot?.entitySource as StorageContainer;
            ItemContainer inventory = BackpackContainer;
            if (inventory == null)
            {
                inventory = lootSource.inventory;
            }

            // sort by name//category, category//name
            if (CustomPlayerOptions[player.userID].SortType == "N")
                inventory.itemList = inventory.itemList.OrderBy(x => x.info.displayName.translated).ThenBy(x => x.info.category.ToString()).ToList();
            else if (CustomPlayerOptions[player.userID].SortType == "C")
                inventory.itemList = inventory.itemList.OrderBy(x => x.info.category.ToString()).ThenBy(x => x.info.displayName.translated).ToList();

            ItemContainer container = new ItemContainer();
            container.ServerInitialize(null, inventory.capacity);
            if ((int)container.uid == 0)
                container.GiveUID();
            container.playerOwner = player;

            // move and stack items to temporary container
            while (inventory.itemList.Count > 0)
            {
                inventory.itemList[0].MoveToContainer(container);
            }

            // move back to source container
            while (container.itemList.Count > 0)
            {
                container.itemList[0].MoveToContainer(inventory);
            }

            inventory.MarkDirty();
        }

        private void OnLootEntity(BasePlayer player, StorageContainer storage)
        {
            if ((BackpackContainer == null && storage.inventory == null) || storage.OwnerID == 0 || !permission.UserHasPermission(player.UserIDString, Perm))
                return;

            BasePlayer owner = null;
            if (BackpackOwnerID != 0)
            {
                owner = BasePlayer.FindByID(BackpackOwnerID);
            }
            else
            {
                owner = BasePlayer.FindByID(storage.OwnerID);
            }

            if (owner == null)
                return;

            if (owner.userID != player.userID && (owner.currentTeam != player.currentTeam && owner.currentTeam != 0))
                return;

            if (CustomPluginConfig.ContainsKey(storage.ShortPrefabName) || BackpackOwnerID != 0)
            {
                var dropBox = storage as DropBox;
                if (dropBox != null)
                {
                    if (dropBox.PlayerBehind(player))
                    {
                        if (BackpackOwnerID != 0)
                        {
                            CreateButtonUI(player, CustomPluginConfig["backpack"]);                            
                        }
                        else
                        {
                            CreateButtonUI(player, CustomPluginConfig[storage.ShortPrefabName]);
                        }
                    }
                }
                else
                {
                    if (BackpackOwnerID != 0)
                    {
                        CreateButtonUI(player, CustomPluginConfig["backpack"]);                            
                    }
                    else
                    {
                        CreateButtonUI(player, CustomPluginConfig[storage.ShortPrefabName]);
                    }
                }
            }
        }

        private void OnLootEntityEnd(BasePlayer player, BaseCombatEntity entity)
        {
            CuiHelper.DestroyUi(player, IdSortButton);
            CuiHelper.DestroyUi(player, IdOrderButton);
        }

        private void OnBackpackOpened(BasePlayer player, ulong backpackOwnerID, ItemContainer backpackContainer)
        {
            if (permission.UserHasPermission(player.UserIDString, Perm))
            {
                BackpackContainer = backpackContainer;
                BackpackOwnerID = backpackOwnerID;
                CreateButtonUI(player, CustomPluginConfig["backpack"]);
            }
        }

        private void OnBackpackClosed(BasePlayer player, ulong backpackOwnerID, ItemContainer backpackContainer)
        {
            BackpackContainer = null;
            BackpackOwnerID = 0;
            CuiHelper.DestroyUi(player, IdSortButton);
            CuiHelper.DestroyUi(player, IdOrderButton);
        }
    }
}